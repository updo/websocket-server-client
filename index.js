const WebSocketServer = require("websocket").server;
const http = require("http");
const { interval, from } = require("rxjs");
const { map, flatMap } = require("rxjs/operators");
const PORT = 9999;

var server = http.createServer(function(request, response) {
  // process HTTP request. Since we're writing just WebSockets
  // server we don't have to implement anything.
});
server.listen(PORT, () => {
  console.log(`SERVER OPEN PORT ${PORT} SUCCESSFULLY`);
});

// create the server
const wsServer = new WebSocketServer({
  httpServer: server
});

const subscription =
interval(1000)
.pipe(
  map(i => [`call-${i + 1}`, `call-${i + 2}`]),
  flatMap(data => from(data.reverse()))
)
.subscribe(data => {
  const i = +data.split("-")[1];
  const msg = { ID: `call__${i}`, duration: i };
  console.log(msg);
  wsServer.broadcastUTF(JSON.stringify(msg));
});

// WebSocket server
wsServer.on("request", request => {
  const connection = request.accept(null, request.origin);
  console.log("request event");

  connection.on("message", message => {
    console.log("message event fired");

    if (message.type === "utf8") {
      // process WebSocket message
      // const body = message.utf8Data;
    }
  });

  connection.on("close", (code, description) => {
    // close user connection
    console.log(
      `Close User Connection Code ${code} et description ${description}`
    );
  });
});

process.on("SIGTERM", () => {
  closeConnexion();
});
process.on("SIGINT", () => {
  closeConnexion();
});

function closeConnexion() {
  console.info("exit Signal Received.");
  subscription.unsubscribe();
  wsServer.shutDown();
  server.close(() => {
    console.log("Webserver closed.");
    console.log("close all conexion client ");
  });
  process.exit();
}
