# AheevaTest
## Start Application

### Install depencies
 Run `npm install` Or `yarn`
### Start NodeJs Application
 Avant de lancer l'application s'assurer que le port 9999 n'est pas utilisé
 Run `node index.js`
### Start Web Client Application
Open a new Terminal  and Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
