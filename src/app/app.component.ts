import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'aheeva-test';
  messages: Array<{ ID: string; duration: number }> = new Array();
  webSocket: WebSocket;
  ngOnInit(): void {
    this.messages = new Array();
  }
  public connectWs() {
    this.webSocket = new WebSocket('ws://localhost:9999');
    this.webSocket.onmessage = event => {
      console.log(event);
      if (event.type === 'message') {
        const newMsg = JSON.parse(event.data);
        this.messages.unshift(newMsg);
      }
    };
    this.webSocket.onclose = event => {
      console.log('close connexion', event);
    };
  }

  ngOnDestroy(): void {
    this.disconnectWs();
  }

  public disconnectWs() {
    this.webSocket.close(1000, 'closed by User');
  }
  /**
   * deleteMsg
   */
  public deleteMsg() {
    this.messages = new Array();
  }
}
